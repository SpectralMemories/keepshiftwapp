package com.spectralmemories.keepshiftw;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    Button wButton;
    Button rwButton;
    List<Button> allButtons;
    TextView statusText;
    ProgressBar progressBar;
    NetworkSender sender;
    boolean initialized = false;
    Thread worker;
    Timer mainTimer;
    Handler mainThreadHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        allButtons = new ArrayList<>();

        mainTimer = new Timer("ConnectionCheck");
        statusText = findViewById(R.id.statusText);
        progressBar = findViewById(R.id.progressBar);
        mainThreadHandler = new Handler();
        initialize();

        wButton = findViewById(R.id.wbutton);
        allButtons.add(wButton);
        wButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendShiftW();
            }
        });

        rwButton = findViewById(R.id.wrbutton);
        allButtons.add(rwButton);
        rwButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendRShiftW();
            }
        });
    }


    public void initialize ()
    {
        if(initialized) return;

        sender = new NetworkSender();
        worker = new Thread(sender);
        worker.start();

        initialized = true;
        statusText.setText(R.string.not_connected);
        statusText.setTextColor(getColor(R.color.colorConnected));
        mainTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if(sender.IsConnected())
                {
                    mainThreadHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            statusText.setText(R.string.connected);
                            statusText.setTextColor(getColor(R.color.colorConnected));
                            progressBar.setVisibility(View.GONE);
                            for (Button b : allButtons)
                            {
                                b.setEnabled(true);
                            }
                        }
                    });
                }
                else
                {
                    mainThreadHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            statusText.setText(R.string.not_connected);
                            statusText.setTextColor(getColor(R.color.colorDisconnected));
                            progressBar.setVisibility(View.VISIBLE);
                            for (Button b : allButtons)
                            {
                                b.setEnabled(false);
                            }
                        }
                    });
                }
            }
        }, 0,2000);
    }

    public void sendShiftW ()
    {
        if(!initialized) return;

        sender.SendMessage("h");
    }

    public void sendRShiftW ()
    {
        if(!initialized) return;

        sender.SendMessage("r");
    }

    public void onDestroy ()
    {
        super.onDestroy();
        if(sender != null && sender.IsConnected())
        {
            sender.SendMessage("q");
            sender.close();
        }
    }
}
