package com.spectralmemories.keepshiftw;
import android.os.Looper;
import android.util.Log;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class NetworkSender implements Closeable, Runnable
{
    public static final int MAX_SUBNET = 255;
    public static final int MIN_PORT_NUMBER = 0;
    public static final int MAX_PORT_NUMBER = 65535;

    private Socket server;
    private DataOutputStream writer;
    private final int PORT = 55980;


    private List<String> toSend;
    private boolean connected;
    private boolean closing;

    NetworkSender ()
    {
        toSend = new ArrayList<>();
        connected = false;
        closing = false;
    }

    void SendMessage (String msg)
    {
        if(!closing) toSend.add(msg);
    }

    boolean IsConnected ()
    {
        return connected;
    }

    private static boolean CheckPort (String host, int port)
    {
        if (port < MIN_PORT_NUMBER || port > MAX_PORT_NUMBER || host == null || host.length() < 8)
        {
            return false;
        }
        // Assume no connection is possible.
        boolean result = false;

        try
        {
            Socket s = new Socket();
            s.connect(new InetSocketAddress(host, port), 50);
            s.close();
            result = true;
        }
        catch(IOException ignored) {}

        return result;
    }

    //This WILL halt execution for a while
    private String getFirstServerIP () throws UnknownHostException
    {
        String ip = "0.0.0.0";

        try(final DatagramSocket socket = new DatagramSocket()){
            socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            ip = socket.getLocalAddress().getHostAddress();
        } catch (SocketException e) {
            e.printStackTrace();
        }

        String[] localIP = ip.split("\\.");
        String subnet = localIP[0] + "." + localIP[1] + "." + localIP[2] + ".";
        for(int i = 0; i <= MAX_SUBNET; i++)
        {
            String t_ip = subnet + String.valueOf(i);
            if(CheckPort(t_ip, PORT))
            {
                return t_ip;
            }
        }

        throw new UnknownHostException("NoClientFound");
    }

    @Override
    public void close()
    {
        closing = true;
        if (server != null)
        {
            new Timer("T_WaitThenClose").schedule(new TimerTask()
            {
                @Override
                public void run() {
                    while (!toSend.isEmpty()){}
                    try {
                        writer.close();
                        server.close();
                    } catch (Exception ignored) {}
                    connected = false;

                }
            },0);
        }
    }


    //In worker thread
    @Override
    public void run()
    {
        Timer timer = new Timer("CheckInternalConnection");
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                try{
                    writer.writeUTF("p");
                    writer.flush();

                    connected = true;
                }catch (Exception e){
                    connected = false;
                }
            }
        }, 4000,2000);
        while(true)
        {
            try
            {
                server = new Socket(getFirstServerIP(), PORT);
                writer = new DataOutputStream(server.getOutputStream());
                server.setSoTimeout(3000);

                connected = true;
                while (connected)
                {
                    if (!toSend.isEmpty())
                    {
                        String s_send = toSend.get(0);
                        if(s_send == null) continue;
                        writer.writeUTF(s_send);
                        writer.flush();
                        toSend.remove(0);
                    }
                }
            }
            catch (Exception e)
            {
                Log.println(Log.ERROR, "NETWORK", e.getMessage());
                StackTraceElement[] elements = e.getStackTrace();
                for (StackTraceElement element: elements)
                {
                    Log.println(Log.ERROR, "NETWORK", "at " + element.getFileName() + ":" + element.getLineNumber());
                }

            }
        }
    }
}
